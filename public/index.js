// questa funzione mi aggiunge il mio bel todo
// dentro il db del mase all'indirizzo 10. bla bla bla 
// usando il mio web server lumen che fa tante cose belle 
async function addTodo(todoDescription){
    // devo fare una chiamata HTTP post
    // che simula quelle che fa postman
    var newTodo={
        description: todoDescription
    }
    await fetch('http://localhost:8000/api/todos', {
        method:'POST',
        headers:{'Content-Type':'aaplication/json'},
        body: JSON.stringify(newTodo)
    });

    // dopo aver aggiunto il todo ricarico la lista
    reloadTodos();
}

// questa riceve l'id del todo ed esegue la chiamata
// HTTP => API/TODOS/{id} [DELETE]
async function deleteTodo(id){
    await fetch('http://localhost:8000/api/todos/'+id, {
        method:'DELETE',
        headers:{'Content-Type':'aaplication/json'},
    });
    // dopo aver eliminato il todo ricarico la lista
    reloadTodos();
}

async function reloadTodos(){
    //ricarico i todos via HTTP con la funzione fetch
    var todosResponse=await fetch('http://localhost:8000/api/todos');
    var todosJson=await todosResponse.json();

    //creo la lista UL in javascript 
    var ulist=document.getElementById('todosList');
    ulist.innerHTML="";

    //itero gli elementi e li aggiungo all'UL
    for(var i=0; i < todosJson.length; i++){
        // creo una variabile con l'elemento i-esimo della lista (il singolo todo)
        var todo=todosJson[i];
        //creo tag LI
        var liElement=document.createElement('li');
        var spanElement=document.createElement('span');
        var removeButton=document.createElement('button');
        removeButton.innerText="X";
        // aggiungiamo l'evento click sul bottone removeButton
        // che quando clicca esegue la funzione removeTodo
        // creo una nuova funzione con il parametro id bindato
        // in modo da richiamare la funzione deleteTodo con quell'id fisso
        var removeThisTodo=deleteTodo.bind(null, todo.id);
        removeButton.addEventListener('click', removeThisTodo);

        // metto la descrizione come contenuto del LI
        spanElement.textContent='#'+todo.id+' '+todo.description;
        spanElement.textContent+='(';
        if(todo.done){
            spanElement.textContent+='Completato';
        }else{
            spanElement.textContent+='Da completare';
        }
        spanElement.textContent+=')';
        // aggiungiamo il bottone
        liElement.appendChild(removeButton);
        liElement.appendChild(spanElement);
        // lo appendiamo alla lista
        ulist.appendChild(liElement);
    }
}

//da qui parte la nostra app JS
//la parola async dice al browser che
//all'interno di questa funzione viene usata
//un operazione asincrona (await)
async function main(){
    console.log("runnin main")
    //prendo il tag div con id app
    var divApp=document.getElementById("app");
    //creo un tag tittle
    var title=document.createElement('h1');
    //aggiungo il tag all'interno del div
    title.textContent="TODO APP";
    divApp.appendChild(title);
    //prendo i todos via HHTP con la funzione fetch
    //N.B la parola wait permette di rendere sincrona
    //un operazione asincrona
    var todosResponse=await fetch('http://localhost:8000/api/todos');
    var todosJson=await todosResponse.json();

    console.log(todosJson);
    // aggiungo una casella di testo
    var textInput=document.createElement('input');
    textInput.placeholder='Inserisci un nuovo todo';
    // aggiungo un id all'input perchè mi serve più tardi per essere
    // recuperato
    textInput.id='addTodoInput';
    divApp.appendChild(textInput);
    // aggiungo il bottone aggiungi
    var addButton=document.createElement('button');
    addButton.textContent='Aggiungimi';
    divApp.appendChild(addButton);
    // attacco l'evento click del bottone
    addButton.addEventListener('click', function(){
        // recupero le descrizione del nuovo todo
        var todoDesc=document.getElementById('addTodoInput').value;
        // uso la funzione addTodo che definisco sopra
        addTodo(todoDesc);
    })

    //creo la lista UL in javascript 
    var ulist=document.createElement('ul');
    ulist.id="todosList";
    //aggiungo la lista alla fine del container divApp
    divApp.appendChild(ulist);

    reloadTodos();
}

//associo l'evento DOMContentLoaded
//alla funzione main
document.addEventListener("DOMContentLoaded", function(){
    main();
});